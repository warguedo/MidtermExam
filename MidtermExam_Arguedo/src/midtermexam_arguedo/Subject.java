/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam_arguedo;

/**
 *
 * @author student
 */
public class Subject {
    private String subjectName;
    private String schedule;

    public Subject() {
    }

    public Subject(String subjectName, String schedule) {
        this.subjectName = subjectName;
        this.schedule = schedule;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }
    
    public void displaySubject() {
        System.out.printf(String.format("Suject"));
        System.out.printf(String.format("\n %-12s : %s ", "Title ", getSubjectName()));
        System.out.printf(String.format("\n %-12s : %s ", "Sched ", getSchedule()));
    
    }
}
