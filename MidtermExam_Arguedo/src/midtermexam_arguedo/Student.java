/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam_arguedo;

/**
 *
 * @author student
 */
public class Student {

    private String id;
    private String firstname;
    private String lastname;

    public Student() {
    }

    public Student(String id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

//    public void displayStudents() {
//        System.out.printf(String.format("\nStudent INFORMATION"));
//        System.out.printf(String.format("\n %-12s : %s ", "Id number ", getId()));
//        System.out.printf(String.format("\n %-12s : %s ", "Firstname ", getFirstname()));
//        System.out.printf(String.format("\n %-12s : %s ", "Lastname ", getFirstname()));
//
//    }
}
