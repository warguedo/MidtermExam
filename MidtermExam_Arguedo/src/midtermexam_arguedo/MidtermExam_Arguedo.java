/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package midtermexam_arguedo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author student
 */
public class MidtermExam_Arguedo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        ArrayList<Student> studentList = new ArrayList<>();
        LinkedHashSet<Student> linkStudentList = new LinkedHashSet<>();
        LinkedList<Student> tempLinkStudent = new LinkedList<>();
        HashMap<String, Student> sortedStudent = new HashMap<String, Student>();

        studentList.add(new Student("101", "Wilmar", "Arguedo"));
        studentList.add(new Student("106", "aaa", "Aaa"));
        studentList.add(new Student("103", "Jecson", "Caracut"));
        studentList.add(new Student("104", "Khim", "Cole"));
        studentList.add(new Student("102", "Ian Jon", "Baid"));
        studentList.add(new Student("105", "Khert", "Geverola"));

        Iterator itr = studentList.iterator();
        while (itr.hasNext()) {
            linkStudentList.add((Student) itr.next());

        }

        itr = linkStudentList.iterator();
        int index = 0;

        while (itr.hasNext()) {
            Student temp = (Student) itr.next();
            if (tempLinkStudent.isEmpty()) {
                tempLinkStudent.add(temp);

            } else {

                index = 0;
                for (Student newTemp : tempLinkStudent) {
                    if (temp.getId().equals(newTemp.getId())) {

                        break;
                    }
                    index++;
                }

                tempLinkStudent.add(index, temp);
            }

        }
        
        
  

        for (Student temp : tempLinkStudent) {
            sortedStudent.put(temp.getId(), temp);

        }

        System.out.println("Sorted Students (Family Name)\n");
        for (Map.Entry map : sortedStudent.entrySet()) {
            Student temp = (Student) map.getValue();

            System.out.println(temp.getLastname() + " " + temp.getFirstname());

        }
        System.out.println("\nSorted Students (Alphabetical Order)\n Id\tLastname\tFirstname\t");

        for (Map.Entry m : sortedStudent.entrySet()) {
            Student temp = (Student) m.getValue();
            System.out.println(temp.getId() + "     " + temp.getLastname() + "       " + temp.getFirstname());
        }

    }

}
